function load_datetime_picker(){
    $("#datetimepicker1").datetimepicker({
        locale: 'pt',
        format: 'DD/MM/YYYY HH:mm',
    });
    $("#datetimepicker2").datetimepicker({
        locale: 'pt',
        format: 'DD/MM/YYYY HH:mm',
    });
    $("#manual_date :input").attr("disabled", true);
}

function draw_graph(json){
    var json_data = json;

    if (json_data) {
        mpld3.draw_figure("fig_01", json_data);
    }
}

function selectMeasurementsToday(){
    $("#past_day").slideUp();
    $("#manual_date").slideUp();
    $("#past_day_button").removeClass('active');
    $("#manual_date_button").removeClass('active');
    $("#manual_date :input").attr("disabled", true);
    $("#past_day :input").attr("disabled", true);
}

function showMeasurementsDays(){
    if($(this).hasClass('active')){
        $("#past_day").slideUp();
        $("#past_day :input").attr("disabled", true);

        $(this).removeClass('active');
    } else {
        $("#past_day :input").attr("disabled", false);
        $("#manual_date :input").attr("disabled", true);
        $("#past_day").slideDown();

        $(this).addClass('active');
    }

    $("#id_current_measurement").removeAttr("checked");
    $("#manual_date").slideUp();
    $("#manual_date_button").removeClass('active');
}

function showManualDate(){
    if($(this).hasClass('active')){
        $("#manual_date").slideUp();
        $("#manual_date :input").attr("disabled", true);

        $(this).removeClass('active');
    } else {
        $("#manual_date :input").attr("disabled", false);
        $("#past_day :input").attr("disabled", true);

        var d = new Date();
        var month = d.getMonth();
        var day = d.getDate();
        var year = d.getFullYear();

        $("#datetimepicker1").data('DateTimePicker')
                             .date(new Date(year, month, day, 00, 00));
        $("#datetimepicker2").data('DateTimePicker')
                             .date(new Date());

        $("#manual_date").slideDown();
        $(this).addClass('active');
    }

    $("#id_current_measurement").removeAttr("checked");
    $("#past_day").slideUp();
    $("#past_day_button").removeClass('active');
}